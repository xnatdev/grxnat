package org.nrg.xnat.pogo.containers

class ContainerLogPollResponsePre322 {
    public static final long LOG_COMPLETE_TIMESTAMP = -1

    String content
    boolean fromFile
    long timestamp
}
