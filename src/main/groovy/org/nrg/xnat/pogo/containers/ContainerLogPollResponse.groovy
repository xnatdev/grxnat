package org.nrg.xnat.pogo.containers

class ContainerLogPollResponse {
    public static final String LOG_COMPLETE_TIMESTAMP = "-1"

    String content
    boolean fromFile
    String timestamp
}
