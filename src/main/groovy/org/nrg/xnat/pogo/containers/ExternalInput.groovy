package org.nrg.xnat.pogo.containers

// bare bones object, can be filled in later if necessary
class ExternalInput {

    String name
    String label
    String description

}
