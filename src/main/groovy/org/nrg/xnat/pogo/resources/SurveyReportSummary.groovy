package org.nrg.xnat.pogo.resources

class SurveyReportSummary {

    String projectId
    int totalEntries
    int totalUids
    int totalDuplicates
    int totalBadFiles
    int totalMismatchedFiles
    int totalMovedFiles
    int totalRemovedFiles
    int totalFileErrors
    String overallStatus

}