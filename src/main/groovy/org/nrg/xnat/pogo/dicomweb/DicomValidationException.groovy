package org.nrg.xnat.pogo.dicomweb

class DicomValidationException extends Exception {

    DicomValidationException(String message) {
        super(message)
    }

}
