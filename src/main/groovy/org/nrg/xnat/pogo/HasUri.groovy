package org.nrg.xnat.pogo

interface HasUri {

    String getUri()

}