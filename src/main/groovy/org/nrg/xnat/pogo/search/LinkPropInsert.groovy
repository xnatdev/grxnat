package org.nrg.xnat.pogo.search

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class LinkPropInsert {

    String name
    String value

}
