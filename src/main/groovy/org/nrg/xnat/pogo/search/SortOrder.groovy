package org.nrg.xnat.pogo.search

enum SortOrder {
    ASC, DESC
}