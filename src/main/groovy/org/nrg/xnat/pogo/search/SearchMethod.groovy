package org.nrg.xnat.pogo.search

enum SearchMethod {
    AND, OR
}