package org.nrg.xnat.pogo.search

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class ColumnCount {

    String values
    int count

}
