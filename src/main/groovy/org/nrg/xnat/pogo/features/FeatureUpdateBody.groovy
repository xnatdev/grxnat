package org.nrg.xnat.pogo.features

class FeatureUpdateBody {

    String key
    Boolean banned
    Boolean enabled

}
