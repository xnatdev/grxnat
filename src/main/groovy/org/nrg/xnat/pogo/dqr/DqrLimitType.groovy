package org.nrg.xnat.pogo.dqr

enum DqrLimitType {
    NO_LIMIT, RECENT_STUDIES_LIMIT
}