package org.nrg.xnat.pogo.dqr

enum CMoveStatus {

    QUEUED, PROCESSING, ISSUED, FAILED, RECEIVED

}