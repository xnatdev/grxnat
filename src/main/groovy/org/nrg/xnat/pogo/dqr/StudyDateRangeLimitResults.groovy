package org.nrg.xnat.pogo.dqr

class StudyDateRangeLimitResults {
    DqrLimitType limitType
    DqrDateRange dateRange
    String limitExplanation
}
