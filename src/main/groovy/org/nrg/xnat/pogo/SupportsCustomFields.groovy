package org.nrg.xnat.pogo

trait SupportsCustomFields {

    Map<String, Object> customFields

}