package org.nrg.xnat.versions.plugins

enum PluginDependencyCheckState {

    SATISFIED,
    MISSING_PLUGIN,
    VERSION_MISMATCH

}