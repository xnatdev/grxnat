package org.nrg.xnat.versions

import org.nrg.xnat.interfaces.XnatInterface
import org.nrg.xnat.interfaces.XnatInterface_1_8_0

@Follows(Xnat_1_8_4)
class Xnat_1_8_5 extends XnatVersion {

    @Override
    List<String> getVersionKeys() {
        ['1.8.5', '1.8.5.1', '1.8.5.2']
    }

    @Override
    Class<? extends XnatInterface> getInterfaceClass() {
        XnatInterface_1_8_0
    }

}
