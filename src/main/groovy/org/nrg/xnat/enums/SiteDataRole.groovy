package org.nrg.xnat.enums

enum SiteDataRole {
    NONE, ALL_DATA_ADMIN, ALL_DATA_ACCESS
}