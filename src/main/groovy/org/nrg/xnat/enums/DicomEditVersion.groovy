package org.nrg.xnat.enums

enum DicomEditVersion {
    DE_4, DE_6, UNSPECIFIED
}
